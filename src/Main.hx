/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

import actions.Action;
import actions.Run;
import h2d.Font;
import h2d.Interactive;
import h2d.Layers;

class Main extends hxd.App {
	public var screenHeight(get, null):Float;
	public var screenWidth(get, null):Float;
	public var leftEdge(get, null):Float;

	private function get_screenHeight():Float {
		return engine.height;
	}

	private function get_screenWidth():Float {
		return engine.width;
	}

	private function get_leftEdge():Float {
		return (screenWidth - s2d.width) / 2;
	}

	public static function main() {
		#if js
		hxd.Res.initEmbed();
		#else
		hxd.Res.initLocal();
		#end
		game = new Main();
	}

	override function loadAssets(onLoaded:Void->Void) {
		Data.load(hxd.Res.data.entry.getText());
		hxd.Res.data.watch(function() {
			Data.load(hxd.Res.data.entry.getText());
			reloadCdb = true;
		});
		onLoaded();
	}

	override function init() {
		#if hl
		hxd.System.onReload = this.flash;
		#end

		engine.backgroundColor = 0xffffff;
		s2d.scaleMode = Fixed(SCREEN_WIDTH, SCREEN_HEIGHT, 1, Center, Center);

		layers = new Layers(s2d);
		blocker = new Interactive(SCREEN_WIDTH, SCREEN_HEIGHT);
		blocker.cursor = hxd.Cursor.Default;
		blocker.visible = false;
		layers.add(blocker, SPEECH_LAYER);

		font = hxd.Res.AmaticSC.toSdfFont(48, SDFChannel.MultiChannel, 0.5, 4 / 24);
		speech = new Speech(font);
		layers.add(speech, SPEECH_LAYER);

		bedroom = new BedroomDay(font);
		layers.add(bedroom, DAY_BEDROOM_LAYER);
	}

	override function update(dt:Float) {
		#if hl
		if (flashTime > 0) {
			flashTime -= dt;
			if (flashTime <= 0) {
				engine.backgroundColor = originalBackground;
				reloadCdb = true;
			}
		}
		#end

		if (reloadCdb) {
			reloadCdb = false;
			bedroom.load();
			if (speech.visible) {
				speech.rebuild();
			}
		}

		while (!actions.isEmpty()) {
			final action = actions.first();
			if (action.update(dt)) {
				break;
			}
			actions.pop();
		}
	}

	public function action(action:Action) {
		actions.add(action);
	}

	public function run(fn:(Float) -> Bool) {
		actions.add(new Run(fn));
	}

	public function clearActionQueue() {
		actions.clear();
	}

	public function block() {
		blocker.visible = true;
	}

	public function unblock() {
		blocker.visible = false;
	}

	#if hl
	private function flash() {
		flashTime = 0.125;
		originalBackground = engine.backgroundColor;
		engine.backgroundColor = 0xff0000;
	}
	#end

	public static final SCREEN_WIDTH = 960;
	public static final SCREEN_HEIGHT = 625;

	public static final DAY_BEDROOM_LAYER = 0;
	public static final SCENE_LAYER = 1;
	public static final SPEECH_LAYER = 2;

	public static var game:Main;

	public var speech:Speech;
	public var layers:Layers;

	private var reloadCdb = false;
	private var bedroom:BedroomDay;
	private var font:Font;
	private final actions:List<Action> = new List();

	public var blocker:Interactive;

	#if hl
	private var originalBackground:Int;
	private var flashTime = 0.0;
	#end
}
