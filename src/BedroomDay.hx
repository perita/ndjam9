/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

import actions.ActorTalk;
import actions.BlockEvents;
import actions.DropPencil;
import actions.SwitchAnim;
import actions.PlayMusic;
import actions.Talk;
import actions.UnblockEvents;
import actions.Wait;
import actions.WaitAnim;
import actors.Actor;
import actors.Who;
import h2d.Bitmap;
import h2d.Font;
import h2d.Layers;
import h2d.Text;
import h2d.Tile;
import h2d.TileGroup;

class BedroomDay extends Layers {
	public function new(font:Font) {
		super();

		this.font = font;

		upper = new TileGroup(hxd.Res.items.get(Data.item.all[0].id.toString()));
		add(upper, UPPER_LAYER);

		lower = new TileGroup(hxd.Res.items.get(Data.item.all[0].id.toString()));
		add(lower, LOWER_LAYER);

		helen = new Actor(Who.Helen, this);
		add(helen, ACTORS_LAYER);

		#if !debug
		removed.push(Data.ItemKind.bedroom_ramon);
		#end
		final showIntro = #if debug false #else true #end;
		Main.game.action(new PlayMusic(hxd.Res.bgm_bedroom));
		if (showIntro) {
			final ramon_x = 100;
			final ramon_y = 320;
			Main.game.action(new BlockEvents());
			Main.game.run(setupBlocker);
			Main.game.run(waitClick);
			Main.game.action(new Wait(0.5));
			Main.game.action(new Talk(Who.Ramon, ramon_x, ramon_y, "Helen!"));
			Main.game.action(new SwitchAnim(helen, "listening"));
			Main.game.action(new ActorTalk(helen, "Yes?"));
			Main.game.action(new Talk(Who.Ramon, ramon_x, ramon_y, "Get ready, we are going to visit daddy."));
			Main.game.action(new Wait(1));
			Main.game.action(new ActorTalk(helen, "Can I bring him something?"));
			Main.game.action(new Wait(1));
			Main.game.action(new Talk(Who.Ramon, ramon_x, ramon_y, "OK."));
			Main.game.action(new Talk(Who.Ramon, ramon_x, ramon_y, "But only one thing, and small."));
			Main.game.action(new DropPencil(helen, this));
			Main.game.action(new WaitAnim(helen, "stand_up", false));
			Main.game.action(new SwitchAnim(helen, "standing"));
			Main.game.action(new UnblockEvents());
		}

		load();
	}

	private function setupBlocker(dt: Float): Bool {
		Main.game.blocker.cursor = hxd.Cursor.Button;
		Main.game.blocker.onClick = (e) -> waitStart = false;
		return false;
	}

	private function waitClick(dt: Float): Bool {
		if (waitStart) {
			return true;
		}
		Main.game.blocker.onClick = null;
		Main.game.blocker.cursor = hxd.Cursor.Default;
		return false;
	}

	public function load() {
		for (child in getLayer(LABELS_LAYER)) {
			removeChild(child);
		}
		upper.removeChildren();
		upper.clear();
		lower.removeChildren();
		lower.clear();
		for (item in Data.item.all) {
			if (!removed.contains(item.id)) {
				new InteractiveItem(item, helen, item.region == Data.Item_region.UPPER ? upper : lower, this);
			}
		}
	}

	public function addLabel(label:String, x:Float, y:Float) {
		final text = new Text(font);
		text.textColor = 0x000000f;
		text.letterSpacing = 0;
		text.smooth = true;
		text.text = label;
		text.setPosition(10, 10);

		final background = Tile.fromColor(0xffffff, Std.int(text.textWidth) + 20, Std.int(text.textHeight) + 20, 0.75);
		final label = new Bitmap(background);
		label.setPosition(x - background.width / 2, y - background.height / 2);
		label.visible = false;
		label.addChild(text);
		add(label, LABELS_LAYER);

		return label;
	}

	public function removeItem(id:Data.ItemKind) {
		removed.push(id);
		if (removed.contains(Data.ItemKind.bedroom_drawing_fairy) && removed.contains(Data.ItemKind.bedroom_ruler)) {
			removed.remove(Data.ItemKind.bedroom_ramon);
		}
		load();
	}

	public static final UPPER_LAYER = 0;
	public static final ACTORS_LAYER = 1;
	public static final LOWER_LAYER = 2;
	public static final LABELS_LAYER = 3;

	private final font:Font;
	private final helen:Actor;
	private final upper:TileGroup;
	private final lower:TileGroup;
	private final removed = new Array<Data.ItemKind>();
	private var waitStart = true;
}
