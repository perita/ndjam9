/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package thief;

import actions.Action;
import h2d.Anim;
import h2d.Flow.FlowAlign;

class PlayAnim implements Action {
	public function new(name:String, setup:() -> Anim, align: FlowAlign = Left ) {
		this.name = name;
		this.setup = setup;
		this.align = align;
	}

	public function onlyFirst(): PlayAnim {
		waiting = false;
		return this;
	}

	public function update(dt:Float):Bool {
		if (anim == null) {
			anim = setup();
			loadAnimation();
			anim.onAnimEnd = () -> waiting = false;
		}
		return waiting;
	}

	private function loadAnimation() {
		@:privateAccess hxd.Res.thief.contents = null;
		final tiles = hxd.Res.thief.getAnim(name, align);
		anim.play(tiles, Math.min(anim.currentFrame, tiles.length));
		hxd.Res.thief.watch(this.loadAnimation);
	}

	private final setup:() -> Anim;
	private var waiting = true;
	private final name:String;
	private var anim:Anim;
	private final align:FlowAlign;
}
