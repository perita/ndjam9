/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package thief;

import h2d.Object;
import actions.Action;

class RunAwayFrom implements Action {
	public function new(objects:Array<Object>) {
		this.objects = objects;
	}

	public function update(dt:Float):Bool {
		var waiting = false;
		for (object in objects) {
			object.x -= dt * 400;
			if (object.x > -230 - Main.game.leftEdge) {
				waiting = true;
			}
		}
		if (waiting) {
			return true;
		}
		for (object in objects) {
			object.visible = false;
		}
		return false;
	}

	private final objects:Array<Object>;
}
