/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package thief;

import actions.Action;
import h2d.Object;

class MoveAway implements Action {
	public function new(objects:Array<Object>) {
		this.objects = objects;
	}

	public function update(dt:Float):Bool {
		var waiting = false;
		for (object in objects) {
			object.x += dt * 400;
			if (object.x <= Main.game.screenWidth) {
				waiting = true;
			}
		}
		return waiting;
	}

	private final objects:Array<Object>;
}