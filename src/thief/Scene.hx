/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package thief;

import actions.BlockEvents;
import actions.Clear;
import actions.Hide;
import actions.Restore;
import actions.Run;
import actions.Talk;
import actions.UnblockEvents;
import actions.Wait;
import actions.WalkTo;
import actors.Who;
import actions.PlayMusic;
import h2d.Anim;
import h2d.Bitmap;
import h2d.Flow.FlowAlign;

class Scene {
	public static function play(room:BedroomDay, actor:actors.Actor, x:Float, y:Float) {
		Main.game.action(new WalkTo(actor, x));
		Main.game.action(new BlockEvents());

		final fairy = new Bitmap(hxd.Res.thief.get("scene_drawing"));
		fairy.setPosition(x, y);

		final helen_x = 550;
		final helen_y = 300;
		final raymond_x = 300;
		final raymond_y = 181;
		final ramon_x = 100;
		final ramon_y = 320;

		Main.game.action(new PlayMusic(hxd.Res.bgm_thief));
		Main.game.action(new Hide(room, fairy));

		Main.game.action(new Wait(1));
		Main.game.action(new Talk(Who.Helen, helen_x, helen_y, "This is a painting."));
		Main.game.action(new Talk(Who.Helen, helen_x, helen_y, "No, wait!"));

		final guard_spotlight = new Bitmap(hxd.Res.thief.get("scene_guard_spotlight"));
		guard_spotlight.setPosition(raymond_x - guard_spotlight.tile.width / 2, raymond_y - 10);
		guard_spotlight.visible = false;
		final thief_spotlight = new Bitmap(hxd.Res.thief.get("scene_thief_spotlight"));
		thief_spotlight.setPosition(helen_x - thief_spotlight.tile.width / 2, helen_y - 20);
		thief_spotlight.visible = false;

		var painting:Anim = new Anim([], 15);
		Main.game.action(new PlayAnim("scene_painting", () -> {
			for (c in Main.game.layers.getLayer(Main.SCENE_LAYER)) {
				Main.game.layers.removeChild(c);
			}
			Main.game.layers.add(guard_spotlight, Main.SCENE_LAYER);
			Main.game.layers.add(thief_spotlight, Main.SCENE_LAYER);
			painting.loop = false;
			painting.setPosition(x + fairy.tile.width, y);
			Main.game.layers.add(painting, Main.SCENE_LAYER);
			return painting;
		}, FlowAlign.Right));

		Main.game.action(new Talk(Who.Helen, helen_x, helen_y, "This is a FANTABULOUS painting."));

		Main.game.action(new Talk(Who.Helen, helen_x, helen_y, "And I’m a world-famous thief!"));

		final thief:Anim = new Anim([], 10);
		Main.game.action(new PlayAnim("scene_thief_intro", () -> {
			Main.game.layers.add(thief, Main.SCENE_LAYER);
			thief.loop = false;
			thief.setPosition(helen_x - 132 / 2, helen_y);
			thief_spotlight.visible = true;
			return thief;
		}));
		Main.game.action(new Wait(1));
		Main.game.action(new PlayAnim("scene_thief_stealing", () -> {
			thief.loop = true;
			return thief;
		}));
		Main.game.action(new Talk(Who.Helen, helen_x, helen_y, "I’m going to steal it!"));

		Main.game.action(new PlayAnim("scene_thief_surprise", () -> {
			thief.loop = false;
			return thief;
		}).onlyFirst());
		Main.game.action(new Talk(Who.Raymond, raymond_x, raymond_y, "Not so fast!"));
		final guard:Anim = new Anim([], 10);
		Main.game.action(new PlayAnim("scene_guard_intro", () -> {
			Main.game.layers.add(guard, Main.SCENE_LAYER);
			guard.setPosition(raymond_x - 180 / 2, raymond_y);
			guard_spotlight.visible = true;
			return guard;
		}));
		Main.game.action(new Talk(Who.Raymond, raymond_x, raymond_y, "For I’m the guard of this museum."));

		Main.game.action(new PlayAnim("scene_thief_running", () -> {
			thief.loop = true;
			return thief;
		}).onlyFirst());
		Main.game.action(new Talk(Who.Helen, helen_x + 10, helen_y, "You’ll never catch me!"));
		Main.game.action(new Talk(Who.Raymond, raymond_x, raymond_y, "Are you sure?"));
		Main.game.action(new Talk(Who.Helen, helen_x + 10, helen_y, "Try!"));

		final end_raymond_x = helen_x + 80;
		final end_raymond_y = raymond_y;
		final end_helen_x = helen_x + 180;
		final end_helen_y = helen_y - 20;

		Main.game.action(new RunAwayFrom([painting, guard, guard_spotlight]));
		Main.game.action(new MoveAway([thief, thief_spotlight]));
		Main.game.action(new Wait(1));
		Main.game.action(new Talk(Who.Raymond, end_raymond_x, end_raymond_y, "Gotcha!"));
		Main.game.action(new Talk(Who.Helen, end_helen_x, end_helen_y - 20, "HAHAHAHA!"));
		Main.game.action(new Talk(Who.Ramon, ramon_x, ramon_y, "Don’t excite her that much!"));

		Main.game.action(new PlayMusic(hxd.Res.bgm_bedroom));
		Main.game.action(new Clear());
		final end = new Bitmap(hxd.Res.thief_bedroom.toTile());
		final raymond = new Bitmap(hxd.Res.actors.get("raymond_standing"));
		Main.game.action(new Run((dt) -> {
			Main.game.layers.add(end, Main.SCENE_LAYER);
			Main.game.layers.add(raymond, Main.SCENE_LAYER);
			raymond.setPosition(end_raymond_x - 180 / 2, end_raymond_y);
			raymond.tile.flipX();
			raymond.tile.dx = -raymond.tile.dx - raymond.tile.width;
			return false;
		}));

		Main.game.action(new Wait(1));
		Main.game.action(new Talk(Who.Ramon, ramon_x, ramon_y, "Then she has trouble falling asleep."));
		Main.game.action(new Talk(Who.Raymond, end_raymond_x, end_raymond_y, "Yes, dear."));
		Main.game.action(new Run((dt) -> {
			raymond.tile.flipX();
			raymond.tile.dx = -raymond.tile.dx - raymond.tile.width;
			return false;
		}));
		Main.game.action(new Talk(Who.Raymond, end_raymond_x, end_raymond_y, "You’ve heard, pumpkin;\nYou’ll steal it another day."));
		Main.game.action(new Talk(Who.Helen, end_helen_x, end_helen_y - 20, "Uh-huh."));
		Main.game.action(new Talk(Who.Raymond, end_raymond_x, end_raymond_y, "’Night, honey."));
		Main.game.action(new Talk(Who.Helen, end_helen_x, end_helen_y - 20, "’Night, daddy."));

		Main.game.action(new Run((dt) -> {
			room.removeItem(Data.ItemKind.bedroom_drawing_fairy);
			return false;
		}));
		Main.game.action(new Restore(room, [end, raymond]));
		Main.game.action(new Run((dt) -> {
			for (c in Main.game.layers.getLayer(Main.SCENE_LAYER)) {
				Main.game.layers.removeChild(c);
			}
			return false;
		}));

		Main.game.action(new UnblockEvents());
	}
}
