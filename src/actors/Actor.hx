/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actors;

import h2d.Anim;
import h2d.Object;

class Actor extends Anim {
	public final who:Who;
	public var dir(default, set) = -1;
	public var speechX(get, never):Float;
	public var speechY(get, never):Float;

	public function new(who:Who, room:BedroomDay, ?parent:Object) {
		super([], 5, parent);
		this.who = who;
		this.name = Std.string(who).toLowerCase();
		this.room = room;
		setPosition(320, 335);
		loadAnimation();
	}

	private function loadAnimation() {
		@:privateAccess hxd.Res.actors.contents = null;
		final tiles = hxd.Res.actors.getAnim(name + "_" + animation);
		play(tiles, Math.min(currentFrame, tiles.length));
		if (dir < 0) {
			flipAnimation(this);
		}
		hxd.Res.actors.watch(this.loadAnimation);
	}

	public function setAnimation(animation:String, loop = true) {
		if (this.animation == animation) {
			return;
		}
		this.currentFrame = 0;
		this.animation = animation;
		switch (animation) {
			case "stand_up":
				speed = 10;
			default:
				speed = 5;
		}
		loadAnimation();
		this.loop = loop;
	}

	public static function flipAnimation(animation:Anim) {
		for (frame in animation.frames) {
			if (frame == null)
				continue;
			frame.flipX();
			frame.dx = -frame.dx - frame.width;
		}
	}

	private function set_dir(dir:Int):Int {
		if (this.dir != dir) {
			flipAnimation(this);
			this.dir = dir;
		}
		return dir;
	}

	private function get_speechX():Float {
		return x + 65;
	}

	private function get_speechY():Float {
		return y + (animation == "listening" ? 20 : 0);
	}

	private final room:BedroomDay;
	private var animation = "playing";
}
