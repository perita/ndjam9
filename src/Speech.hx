/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

import actors.Who;
import h2d.Bitmap;
import h2d.Interactive;
import h2d.Text;
import h2d.ScaleGrid;
import h2d.Font;
import h2d.Object;

class Speech extends Interactive {
	public function new(font:Font, ?parent:Object) {
		super(Main.SCREEN_WIDTH, Main.SCREEN_WIDTH, parent);
		visible = false;

		bubble = new ScaleGrid(hxd.Res.items.get("speech_bubble"), 32, 32, 32, 32, this);
		tail = new Bitmap(hxd.Res.items.get("speech_tail"), this);

		this.text = new Text(font, bubble);
		text.setPosition(32, 0);
		text.textColor = 0x000000f;
		text.letterSpacing = 0;
		text.smooth = true;
	}

	public function show(who:Who, x:Float, y:Float, line:String) {
		bubble.clear();
		this.who = who;

		final colors = Data.speech_colors.get(switch (who) {
			case Helen:
				Data.Speech_colorsKind.helen;
			case Ramon:
				Data.Speech_colorsKind.ramon;
			case Raymond:
				Data.Speech_colorsKind.raymond;
		});
		final backgroundColor = 0xff000000 | colors.background;
		final foregroundColor = 0xff000000 | colors.foreground;

		text.textAlign = Align.Center;
		text.text = line;
		text.color.setColor(foregroundColor);

		bubble.width = text.textWidth + 64;
		text.x = bubble.width / 2;
		bubble.height = Math.max(64, text.textHeight + 12);
		bubble.color.setColor(backgroundColor);
		bubble.setPosition(Math.max(15, Math.min(x - bubble.width / 2, Main.SCREEN_WIDTH - bubble.width)), y - bubble.height - tail.tile.height);

		tail.color.setColor(backgroundColor);
		tail.setPosition(x - tail.tile.width / 2, y - tail.tile.height);

		visible = true;
	}

	public function rebuild() {
		show(this.who, tail.x + tail.tile.width / 2, tail.y + tail.tile.height, text.text);
	}

	override function onPush(e:hxd.Event) {
		visible = false;
	}

	private final text:Text;
	private final bubble:ScaleGrid;
	private final tail:Bitmap;
	private var who:Who;
}
