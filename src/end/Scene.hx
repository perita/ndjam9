/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package end;

import actions.PlayMusic;
import h2d.Anim;
import h2d.Object;
import h2d.Tile;
import h2d.Bitmap;
import actors.Who;
import actions.Talk;
import actors.Actor;
import h2d.Text;
import h2d.Font;
import actions.WalkTo;
import h2d.Interactive;
import actions.Wait;
import actions.BlockEvents;
import actions.UnblockEvents;

class Scene {
	public function new(room:BedroomDay, actor:Actor, x:Float, y:Float) {
		this.room = room;
		this.actor = actor;
		this.x = x;
		this.y = y;
		this.helen = new Anim([], 5);
		this.raymond = new Anim([], 5);
	}

	public function play() {
		Main.game.action(new WalkTo(actor, x + 150));
		Main.game.action(new BlockEvents());

		Main.game.run((dt) -> ramonSays(dt, "Are you ready? Did you find\nsomething to bring him?"));
		Main.game.run(chooseItem);
		Main.game.run((dt) -> ramonSays(dt, "This?"));
		Main.game.action(new Wait(1));
		Main.game.run((dt) -> ramonSays(dt, "Very well. Off we go."));
		Main.game.run(leave);
		Main.game.action(new Wait(3));
		Main.game.run(arrive);
		Main.game.action(new Wait(1));
		Main.game.run((dt) -> doorSays(dt, "Doctor, a word?"));
		Main.game.action(new Wait(0.5));
		Main.game.run((dt) -> raymondSays(dt, "So, what's cookin', kiddo?"));
		Main.game.run((dt) -> helenSays(dt, "I’ve brought you something."));
		Main.game.run(give);
		Main.game.run((dt) -> raymondSays(dt, "What’s this?"));
		Main.game.run(stand);
		Main.game.action(new Wait(0.5));
		Main.game.run((dt) -> raymondMaySay(dt, "Oh, a “swashbuckler’s sword”, huh?", "Oh, a “fantabulous painting”, huh?"));
		Main.game.run((dt) -> helenSays(dt, "Yes!"));
		Main.game.run((dt) -> raymondMaySay(dt, "Did you know? Once upon a time,\nthere was an old swordsman.",
			"Did you know? Once upon a time,\nthere was an old painter."));
		Main.game.run(enterStory);
		Main.game.run((dt) -> raymondMaySay(dt, "He won many fierce battles\nand reached a very old age.",
			"He painted many wonderful paintings\nand reached a very old age."));
		Main.game.run(frown);
		Main.game.run((dt) -> raymondSays(dt, "One day, in his deathbed,\nhe said to his daughter:"));
		Main.game.run(well_up);
		Main.game.run((dt) -> raymondSays(dt, "“Daughter, I am dying. And...”"));
		Main.game.action(new PlayMusic(hxd.Res.bgm_end));
		Main.game.run(throw_herself);
		Main.game.run(leaveStory);
		Main.game.run((dt) -> helenSays(dt, "NOOOO!"));
		Main.game.run((dt) -> raymondSays(dt, "Helen!"));
		Main.game.action(new Wait(1));
		Main.game.run((dt) -> helenSays(dt, "I don’t want you to die."));
		Main.game.run((dt) -> raymondSays(dt, "I didn’t..."));
		Main.game.run((dt) -> raymondSays(dt, "I’m not..."));
		Main.game.run((dt) -> raymondSays(dt, "Sigh. I’m sorry, Helen--I wasn’t thinking."));
		Main.game.action(new Wait(1));
		Main.game.run((dt) -> raymondSays(dt, "Don’t worry: I’m gonna stick around\nfor a very long time."));
		Main.game.run((dt) -> helenSays(dt, "Promise?"));
		Main.game.run(fade);
		Main.game.action(new Wait(1));
		Main.game.run((dt) -> raymondSays(dt, "Promise."));
		Main.game.action(new Wait(1));
		Main.game.run(theEnd);
		#if debug
		Main.game.run(comeBack);
		Main.game.action(new UnblockEvents());
		#end
	}

	private function ramonSays(dt:Float, line:String):Bool {
		return says(dt, Who.Ramon, x + 130 / 2, y, line);
	}

	private function doorSays(dt:Float, line:String):Bool {
		return says(dt, Who.Ramon, 845, 255, line);
	}

	private function raymondSays(dt:Float, line:String):Bool {
		return says(dt, Who.Raymond, raymond.x + raymondOffset, raymond.y + 25, line);
	}

	private function raymondMaySay(dt:Float, rulerLine:String, fairyLine:String):Bool {
		return raymondSays(dt, choosen == Data.ItemKind.bedroom_ruler ? rulerLine : fairyLine);
	}

	private function helenSays(dt:Float, line:String):Bool {
		return says(dt, Who.Helen, helen.x + 130 / 2, helen.y, line);
	}

	private function says(dt:Float, who:Who, x:Float, y:Float, line:String):Bool {
		if (talk == null) {
			talk = new Talk(who, x, y, line);
			return talk.update(dt);
		}
		if (talk.update(dt)) {
			return true;
		}
		talk = null;
		return false;
	}

	private function chooseItem(dt:Float):Bool {
		if (background == null) {
			clearLayer();

			final white = Tile.fromColor(0xffffff, Main.SCREEN_WIDTH, Main.SCREEN_HEIGHT, 0.9);
			final block = new Interactive(white.width, white.height);
			block.cursor = hxd.Cursor.Default;
			addToLayer(block);

			background = new Bitmap(white);
			addToLayer(background);

			final font = hxd.Res.AmaticSC.toSdfFont(64, SDFChannel.MultiChannel, 0.5, 4 / 24);
			final text = new Text(font, background);
			text.textColor = 0x000000f;
			text.letterSpacing = 0;
			text.smooth = true;
			text.text = "Choose an item";
			text.setPosition(Main.SCREEN_WIDTH / 2 - text.textWidth / 2, 50);

			final ruler = new Bitmap(hxd.Res.items.get("bedroom_ruler_big"));
			final ruler_interactive = new Interactive(ruler.tile.width, ruler.tile.height, background);
			ruler_interactive.setPosition(Main.SCREEN_WIDTH / 4 - ruler_interactive.width / 2, Main.SCREEN_HEIGHT / 2 - ruler_interactive.height / 2);
			ruler_interactive.addChild(ruler);
			ruler_interactive.onPush = (e) -> choosen = Data.ItemKind.bedroom_ruler;

			final fairy = new Bitmap(hxd.Res.items.get("bedroom_drawing_fairy_big"));
			final fairy_interactive = new Interactive(fairy.tile.width, fairy.tile.height, background);
			fairy_interactive.setPosition(3 * Main.SCREEN_WIDTH / 4 - fairy_interactive.width / 2, Main.SCREEN_HEIGHT / 2 - fairy_interactive.height / 2);
			fairy_interactive.addChild(fairy);
			fairy_interactive.onPush = (e) -> choosen = Data.ItemKind.bedroom_drawing_fairy;

			Main.game.unblock();
		}
		if (choosen != Data.ItemKind.bedroom_ruler && choosen != Data.ItemKind.bedroom_drawing_fairy) {
			return true;
		}
		Main.game.block();
		clearLayer();
		background = null;
		return false;
	}

	private function clearLayer() {
		final layers = Main.game.layers;
		for (c in layers.getLayer(Main.SCENE_LAYER)) {
			layers.removeChild(c);
		}
	}

	private function addToLayer(object:Object) {
		Main.game.layers.add(object, Main.SCENE_LAYER);
	}

	private function leave(dt:Float):Bool {
		room.x += dt * 300;
		if (room.x < Main.game.screenWidth + 10) {
			return true;
		}
		room.visible = false;
		return false;
	}

	private function arrive(dt:Float):Bool {
		if (background == null) {
			background = new Bitmap(hxd.Res.hospital.toTile());
			background.setPosition(-(Main.SCREEN_WIDTH + Main.game.leftEdge) - 10, 0);
			addToLayer(background);

			helen.setPosition(588.179, 296.365);
			playHelen("standing");
			background.addChild(helen);

			raymond.setPosition(357.661, 210.249);
			playRaymond("in_bed");
			background.addChild(raymond);
		}
		background.x = Math.min(0, background.x + dt * 300);
		return background.x < 0;
	}

	private function playHelen(name:String) {
		helen.play(getAnim("helen_" + name));
		Actor.flipAnimation(helen);
	}

	private function playRaymond(name:String) {
		raymond.play(getAnim("raymond_" + name));
	}

	private function getAnim(name:String):Array<Tile> {
		return hxd.Res.actors.getAnim(name);
	}

	private function give(dt:Float):Bool {
		playHelen("pointing");
		playRaymond("receiving");
		return false;
	}

	private function stand(dt:Float):Bool {
		playHelen("standing");
		playRaymond("in_bed");
		return false;
	}

	private function frown(dt:Float):Bool {
		playHelen("frowning");
		return false;
	}

	private function well_up(dt:Float):Bool {
		playHelen("welling_up");
		return false;
	}

	private function throw_herself(dt:Float):Bool {
		helen.visible = false;
		helen.x -= 170;
		helen.y -= 30;
		raymondOffset += 40;
		playRaymond("with_helen");
		return false;
	}

	private function enterStory(dt:Float):Bool {
		if (mask == null) {
			mask = new Bitmap(hxd.Res.items.get("bedroom_mask"), background);
			mask.alpha = 0;
			return true;
		}
		mask.alpha += dt;
		final rgb = 255 - Std.int(Math.min(255, 255 * mask.alpha));
		Main.game.engine.backgroundColor = 0xff000000 | (rgb << 16) | (rgb << 8) | (rgb);
		return mask.alpha < 1;
	}

	private function leaveStory(dt:Float):Bool {
		background.removeChild(mask);
		mask = null;
		Main.game.engine.backgroundColor = 0xffffffff;
		return false;
	}

	private function fade(dt:Float):Bool {
		if (fadeScreen == null) {
			fadeScreen = new Bitmap(Tile.fromColor(0xffffff, Main.SCREEN_WIDTH, Main.SCREEN_HEIGHT));
			fadeScreen.alpha = 0;
			addToLayer(fadeScreen);
			return true;
		}
		fadeScreen.alpha += dt / 2;
		return fadeScreen.alpha < 1;
	}

	private function theEnd(dt:Float):Bool {
		if (endText == null) {
			final font = hxd.Res.AmaticSC.toSdfFont(128, SDFChannel.MultiChannel, 0.5, -1);
			endText = new Text(font, background);
			endText.textColor = 0x000000f;
			endText.letterSpacing = 0;
			endText.smooth = true;
			endText.text = "THE END";
			endText.setPosition(Main.SCREEN_WIDTH / 2 - endText.textWidth / 2, Main.SCREEN_HEIGHT / 2 - endText.textHeight / 2);
			endText.alpha = 0;
			addToLayer(endText);
			return true;
		}
		endText.alpha += dt / 2;
		return endText.alpha < 1;
	}

	#if debug
	private function comeBack(dt:Float):Bool {
		clearLayer();
		room.visible = true;
		room.x = 0;
		return false;
	}
	#end

	private var endText:Text;
	private var mask:Bitmap;
	private var fadeScreen:Bitmap;
	private final raymond:Anim;
	private var raymondOffset = 268.0 / 2.0;
	private final helen:Anim;
	private var background:Bitmap;
	private var talk:Talk;
	private var choosen:Data.ItemKind;
	private final room:BedroomDay;
	private final actor:Actor;
	private final x:Float;
	private final y:Float;
}
