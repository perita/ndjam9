/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

import actors.Actor;

class WaitAnim implements Action {
	public function new(actor:Actor, animation:String, loop = true) {
		this.actor = actor;
		this.animation = animation;
		this.loop = loop;
	}

	public function update(dt:Float):Bool {
		if (!setup) {
			actor.setAnimation(animation, loop);
			oldEnd = actor.onAnimEnd;
			actor.onAnimEnd = () -> {
				waiting = false;
				actor.onAnimEnd = oldEnd;
			}
		}
		return waiting;
	}

	private var oldEnd:() -> Void;
	private var setup = false;
	private var waiting = true;
	private final actor:Actor;
	private final animation:String;
	private final loop:Bool;
}
