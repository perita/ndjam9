/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

class Wait implements Action {
	public function new(delay:Float) {
		this.delay = delay;
	}

	public function update(dt:Float):Bool {
		delay -= dt;
		return delay > 0;
	}

	private var delay:Float;
}
