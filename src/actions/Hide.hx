/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

import h2d.Object;
import h2d.Graphics;
import h2d.Bitmap;
import actions.Action;

class Hide implements Action {
	public function new(room:Object, bitmap:Bitmap) {
		this.room = room;
		this.bitmap = bitmap;
		this.x = bitmap.x + bitmap.tile.width / 2;
		this.y = bitmap.y + bitmap.tile.width / 2;
	}

	public function update(dt:Float):Bool {
		if (!setup) {
			setup = true;
			Main.game.layers.add(g, Main.SCENE_LAYER);
			Main.game.layers.add(bitmap, Main.SCENE_LAYER);
		}
		r += dt * 1000;
		g.clear();
		g.beginFill(BACKGROUND_COLOR);
		g.drawCircle(x, y, r);
		if (r < Main.SCREEN_WIDTH) {
			return true;
		}
		Main.game.layers.removeChild(g);
		Main.game.engine.backgroundColor = BACKGROUND_COLOR;
		room.visible = false;
		return false;
	}

	private final room:Object;
	private var setup = false;
	private var r = 1.0;
	private final bitmap:Bitmap;
	private final g = new Graphics();
	private final x:Float;
	private final y:Float;

	private static final BACKGROUND_COLOR = 0xff000000;
}
