/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

class PlayMusic implements Action {
	public function new(m:hxd.res.Sound) {
		this.m = m;
	}

	public function update(dt:Float):Bool {
		if (old != null) {
			old.stop();
		}
		old = m;
		m.play(true);
		return false;
	}

	private final m:hxd.res.Sound;
	private static var old:hxd.res.Sound;
}
