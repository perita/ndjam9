/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

import actors.Actor;

class SwitchAnim implements Action {
	public function new(actor:Actor, animation:String, loop = true) {
		this.actor = actor;
		this.animation = animation;
		this.loop = loop;
	}

	public function update(dt:Float):Bool {
		actor.setAnimation(animation, loop);
		return false;
	}

	private final actor:Actor;
	private final animation:String;
	private final loop:Bool;
}
