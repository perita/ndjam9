/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

import h2d.Layers;
import h2d.Anim;
import actors.Actor;

class DropPencil implements Action {
	public function new(actor:Actor, layers:Layers) {
		this.actor = actor;
		this.layers = layers;
	}

	public function update(dt:Float):Bool {
		final tiles = hxd.Res.actors.getAnim(actor.name + "_drop_pencil");
		final drop = new Anim(tiles, 15);
		drop.setPosition(actor.x, actor.y);
		drop.loop = false;
		if (actor.dir < 0) {
			Actor.flipAnimation(drop);
		}
		layers.add(drop, BedroomDay.ACTORS_LAYER);
		return false;
	}

	private final actor:Actor;
	private final layers:Layers;
}
