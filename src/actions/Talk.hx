/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

import actors.Who;

class Talk implements Action {
	public function new(who:Who, x:Float, y:Float, line:String) {
		this.who = who;
		this.x = x;
		this.y = y;
		this.line = line;
	}

	public function update(dt:Float):Bool {
		if (!setup) {
			Main.game.speech.show(who, x, y, line);
			setup = true;
			return true;
		}
		return Main.game.speech.visible;
	}

	private final who:Who;
	private final x:Float;
	private final y:Float;
	private final line:String;
	private var setup = false;
}
