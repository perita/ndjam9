/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

class Run implements Action {
	public function new(fn:(Float) -> Bool) {
		this.fn = fn;
	}

	public function update(dt:Float):Bool {
		return fn(dt);
	}

	private final fn:(Float) -> Bool;
}
