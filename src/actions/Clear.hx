/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

import actions.Action;

class Clear implements Action {
	public function new() {}

	public function update(dt:Float):Bool {
		for (c in Main.game.layers.getLayer(Main.SCENE_LAYER)) {
			Main.game.layers.removeChild(c);
		}
		Main.game.engine.backgroundColor = 0xffffffff;
		return false;
	}
}
