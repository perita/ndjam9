/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

import actors.Actor;

class WalkTo implements Action {
	public function new(actor:Actor, x:Float) {
		this.actor = actor;
		this.x = Math.max(Math.min(700, x), 50);
	}

	public function update(dt:Float):Bool {
		if (!setup) {
			if (Math.abs(x - actor.x) < 5) {
				return false;
			}
			setup = true;
			actor.dir = x >= actor.x ? 1 : -1;
			actor.setAnimation("walking");
			return true;
		}
		actor.x += 150 * dt * actor.dir;
		if ((actor.dir > 0 && actor.x < x) || (actor.dir < 0 && actor.x > x)) {
			return true;
		}
		actor.setAnimation("standing");
		return false;
	}

	private final actor:Actor;
	private final x:Float;
	private var setup = false;
}
