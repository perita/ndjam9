/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

import h2d.Object;
import actions.Action;

class Restore implements Action {
	public function new(room:Object, extras:Array<Object>) {
		this.room = room;
		this.extras = extras;
	}

	public function update(dt:Float):Bool {
		if (!room.visible) {
			room.visible = true;
			room.y = Main.game.screenHeight + 10;
		}
		final dy = dt * 300;
		room.y = Math.max(0, room.y - dy);
		for (extra in extras) {
			extra.y -= dy;
		}
		return room.y > 0;
	}

	private final room:Object;
	private final extras:Array<Object>;
}
