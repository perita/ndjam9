/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

class BlockEvents implements Action {
	public function new() {}

	public function update(dt:Float):Bool {
		Main.game.block();
		return false;
	}
}
