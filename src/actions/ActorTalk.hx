/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package actions;

import actors.Actor;

class ActorTalk implements Action {
	public function new(actor:Actor, line:String) {
		this.actor = actor;
		this.line = line;
	}

	public function update(dt:Float):Bool {
		if (talk == null) {
			talk = new Talk(actor.who, actor.speechX, actor.speechY, line);
		}
		return talk.update(dt);
	}

	private final actor:Actor;
	private final line:String;
	private var talk:Talk;
}
