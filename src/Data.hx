/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

 private typedef Init = haxe.macro.MacroType<[cdb.Module.build("data.cdb")]>;