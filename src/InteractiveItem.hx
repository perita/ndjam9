/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

import actions.ActorTalk;
import actions.WalkTo;
import actors.Actor;
import h2d.Bitmap;
import h2d.Interactive;
import h2d.Tile;
import h2d.TileGroup;
import hxd.Event;

class InteractiveItem {
	public function new(item:Data.Item, actor:Actor, group:TileGroup, room:BedroomDay) {
		this.room = room;
		this.item = item;
		this.actor = actor;

		var tile = hxd.Res.items.get(item.id.toString());
		if (tile == null) {
			tile = Tile.fromColor(0xff00ff, 10, 10);
		}
		group.add(item.x, item.y, tile);

		if (item.action != Data.Item_action.NONE) {
			label = room.addLabel(item.label, item.x + tile.width / 2, item.y + tile.height / 2);

			final interactive = new Interactive(tile.width, tile.height, group);
			interactive.setPosition(item.x, item.y);
			interactive.onOver = showLabel;
			interactive.onOut = hideLabel;
			interactive.onPush = act;
		} else {
			label = null;
		}
	}

	private function showLabel(e:Event) {
		if (label != null) {
			label.visible = true;
		}
	}

	private function hideLabel(e:Event) {
		if (label != null) {
			label.visible = false;
		}
	}

	private function act(e:Event) {
		Main.game.clearActionQueue();
		switch (item.action) {
			case DESCRIBE:
				describe();
			case THIEF_STORY:
				thief.Scene.play(room, actor, item.x, item.y);
			case PIRATE_STORY:
				(new pirate.Scene(room, actor, item.x, item.y)).play();
			case LEAVE:
				(new end.Scene(room, actor, item.x, item.y)).play();
			case NONE:
		}
	}

	private function describe() {
		Main.game.action(new WalkTo(actor, item.x));
		Main.game.action(new ActorTalk(actor, StringTools.replace(item.description, "\\n", "\n")));
	}

	private final room:BedroomDay;
	private final item:Data.Item;
	private final label:Bitmap;
	private final actor:Actor;
}
