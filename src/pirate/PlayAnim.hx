/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package pirate;

import actions.Action;
import h2d.Anim;
import h2d.Flow.FlowAlign;

class PlayAnim implements Action {
	public function new(name:String, anim:Anim, align:FlowAlign = Left) {
		this.name = name;
		this.anim = anim;
		this.align = align;
		loadAnimation();
		anim.onAnimEnd = () -> waiting = false;
	}

	public function update(dt:Float):Bool {
		return waiting;
	}

	private function loadAnimation() {
		@:privateAccess hxd.Res.pirate.contents = null;
		final tiles = hxd.Res.pirate.getAnim(name, align);
		anim.play(tiles, Math.min(anim.currentFrame, tiles.length));
		hxd.Res.pirate.watch(this.loadAnimation);
	}

	private var waiting = true;
	private final name:String;
	private final anim:Anim;
	private final align:FlowAlign;
}
