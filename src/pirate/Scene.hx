/*
 * SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

package pirate;

import h2d.Graphics;
import actions.BlockEvents;
import actions.Clear;
import actions.Hide;
import actions.Restore;
import actions.PlayMusic;
import actions.Talk;
import actions.UnblockEvents;
import actions.Wait;
import actions.WalkTo;
import actors.Who;
import actors.Actor;
import h2d.Anim;
import h2d.Bitmap;
import h2d.Object;

class Scene {
	public function new(room:BedroomDay, actor:Actor, x:Float, y:Float) {
		this.room = room;
		this.actor = actor;

		ruler = new Bitmap(hxd.Res.pirate.get("scene_ruler"));
		ruler.setPosition(x, y);

		spotlight = new Bitmap(hxd.Res.pirate.get("scene_spotlight"));
		spotlight.setPosition(335, 150);
		helen = new Anim([], 5, spotlight);
		helen.setPosition(242.0, 8.0);
		raymond = new Anim([], 5, spotlight);
		raymond.setPosition(80.0, 36.0);
		ramon_x -= spotlight.x;
		ramon_y -= spotlight.y;
	}

	public function play() {
		Main.game.action(new WalkTo(actor, ruler.x));
		Main.game.action(new BlockEvents());

		Main.game.action(new PlayMusic(hxd.Res.bgm_pirate));
		Main.game.action(new Hide(room, ruler));
		Main.game.action(new Wait(1));
		Main.game.run(addPirates);
		Main.game.run(throwRuler);
		Main.game.action(new Wait(0.5));
		Main.game.run(revealPirates);
		Main.game.action(new Wait(0.5));
		Main.game.run((dt) -> helenSays(dt, "I’m a pirate."));
		Main.game.run((dt) -> helenSays(dt, "I’m a GOOD pirate."));
		Main.game.run((dt) -> helenSays(dt, "But you are evil!"));
		Main.game.run((dt) -> raymondSays(dt, "Me, evil?\nWhy?"));
		Main.game.run((dt) -> helenSays(dt, "Because you have a..."));
		Main.game.run((dt) -> helenSays(dt, "One of those moustache and beard things."));
		Main.game.run((dt) -> raymondSays(dt, "A goatee?"));
		Main.game.run((dt) -> helenSays(dt, "Yes!"));
		Main.game.action(new Wait(0.25));
		Main.game.run(growGoatee);
		Main.game.action(new Wait(0.75));
		Main.game.run((dt) -> helenSays(dt, "So... EVIL!!"));
		Main.game.run((dt) -> raymondSays(dt, "Curses, foiled again!"));
		Main.game.run((dt) -> raymondSays(dt, "Let’s see if your tongue is as sharp\nas your wit. En garde!"));
		Main.game.run(startFencing);
		Main.game.action(new Wait(2));
		Main.game.run(stopFencing);
		Main.game.run((dt) -> raymondSays(dt, "You fight like a dairy farmer!"));
		Main.game.run((dt) -> helenSays(dt, "How fitting--you fight like a cow!"));
		Main.game.run(startFencing);
		Main.game.action(new Wait(2));
		Main.game.run(stopFencing);
		Main.game.run((dt) -> raymondSays(dt, "Have you stopped wearing diapers yet?"));
		Main.game.run((dt) -> helenSays(dt, "Why? Do you want to borrow one?"));
		Main.game.run(startFencing);
		Main.game.run((dt) -> ramonSays(dt, "Do you really think THIS is APPROPRIATE?"));
		Main.game.action(new Clear());
		Main.game.action(new PlayMusic(hxd.Res.bgm_bedroom));
		Main.game.run(addBedroom);
		Main.game.action(new Wait(0.5));
		Main.game.run(flipRaymond);
		Main.game.run((dt) -> raymondSays(dt, "What? It’s totally innocent."));
		Main.game.run((dt) -> ramonSays(dt, "Is it, now? What’s it called again?"));
		Main.game.run((dt) -> raymondSays(dt, "Insult sword fi..."));
		Main.game.action(new Wait(1));
		Main.game.run((dt) -> raymondSays(dt, "OK, I see your point."));
		Main.game.run(flipRaymond);
		Main.game.run((dt) -> raymondSays(dt, "Time to wrap it up, my little\nbuccaneer."));
		Main.game.action(new Wait(0.5));
		Main.game.run(putHelenToSleep);
		Main.game.action(new Wait(0.75));
		Main.game.run((dt) -> raymondSays(dt, "’Night, sweetie."));
		Main.game.run((dt) -> helenSays(dt, "’Night, daddy."));

		Main.game.run(removeRulerFromRoom);
		Main.game.action(new Restore(room, [background]));

		Main.game.action(new UnblockEvents());
	}

	private function addPirates(dt:Float):Bool {
		addToLayer(spotlight);
		helen.play(hxd.Res.pirate.getAnim("scene_small_standing"));
		raymond.play(hxd.Res.pirate.getAnim("scene_big_standing"));
		g = new Graphics(spotlight);
		drawCircles();
		return false;
	}

	private function drawCircles() {
		g.beginFill(0x000000);
		g.drawCircle(helen.x + 138, helen.y + 120, r_small);
		g.drawCircle(raymond.x + 83, raymond.y + 147, r_big);
	}

	private function addToLayer(object:Object) {
		Main.game.layers.add(object, Main.SCENE_LAYER);
	}

	private function helenSays(dt:Float, line:String):Bool {
		return says(dt, Who.Helen, helen.x + helen_width, helen.y + 20, line);
	}

	private function raymondSays(dt:Float, line:String):Bool {
		return says(dt, Who.Raymond, raymond.x + 100, raymond.y, line);
	}

	private function ramonSays(dt:Float, line:String):Bool {
		return says(dt, Who.Ramon, ramon_x, ramon_y, line);
	}

	private function says(dt:Float, who:Who, x:Float, y:Float, line:String):Bool {
		if (talk == null) {
			talk = new Talk(who, spotlight.x + x, spotlight.y + y, line);
			return talk.update(dt);
		}
		if (talk.update(dt)) {
			return true;
		}
		talk = null;
		return false;
	}

	private function growGoatee(dt:Float):Bool {
		if (playAnim == null) {
			raymond.speed = 15;
			raymond.loop = false;
			playAnim = new PlayAnim("scene_grow_goatee", raymond);
		}
		if (playAnim.update(dt)) {
			return true;
		}
		playAnim = null;
		return false;
	}

	private function startFencing(dt:Float):Bool {
		helen.speed = raymond.speed = 5;
		helen.loop = raymond.loop = true;
		helen.currentFrame = raymond.currentFrame = 0;
		final helen_tiles = hxd.Res.pirate.getAnim("scene_small_fencing");
		helen.play(helen_tiles, Math.min(helen.currentFrame, helen_tiles.length));
		final raymond_tiles = hxd.Res.pirate.getAnim("scene_big_fencing");
		raymond.play(raymond_tiles, Math.min(raymond.currentFrame, raymond_tiles.length));
		return false;
	}

	private function stopFencing(dt:Float):Bool {
		helen.play(hxd.Res.pirate.getAnim("scene_small_standing"), 0);
		raymond.play(hxd.Res.pirate.getAnim("scene_goatee_standing"), 0);
		return false;
	}

	private function addBedroom(dt:Float):Bool {
		addToLayer(background);

		background.addChild(helen);
		helen.loop = false;
		helen.play(hxd.Res.pirate.getAnim("scene_helen_standing"), 0);
		helen.setPosition(spotlight.x + helen.x, spotlight.y + helen.y);

		background.addChild(raymond);
		raymond.loop = false;
		raymond.play(hxd.Res.pirate.getAnim("scene_raymond_standing"), 0);
		raymond.setPosition(spotlight.x + raymond.x, spotlight.y + raymond.y);

		ramon_x += spotlight.x;
		ramon_y += spotlight.y;
		spotlight.setPosition(0, 0);

		return false;
	}

	private function flipRaymond(dt:Float):Bool {
		final dx = raymond.frames[0].dx < 0 ? 0 : -240 / 4;
		for (frame in raymond.frames) {
			if (frame == null)
				continue;
			frame.flipX();
			frame.dx = dx;
		}
		return false;
	}

	private function putHelenToSleep(dt:Float):Bool {
		helen.play(hxd.Res.pirate.getAnim("scene_helen_sleeping"), 0);
		helen_width = helen.frames[0].width / 2.0;
		helen.setPosition(sleep_x, sleep_y);
		return false;
	}

	private function throwRuler(dt:Float):Bool {
		if (playAnim == null) {
			final name = "scene_throw_ruler";
			rulerAnim = new Anim(hxd.Res.pirate.getAnim(name), 10);
			rulerAnim.setPosition(ruler.x + 1, ruler.y - rulerAnim.frames[0].height + ruler.tile.height - 3);
			rulerAnim.loop = false;
			addToLayer(rulerAnim);
			Main.game.layers.removeChild(ruler);
			playAnim = new PlayAnim(name, rulerAnim);
		}
		if (playAnim.update(dt)) {
			return true;
		}
		playAnim = null;
		return false;
	}

	private function revealPirates(dt:Float):Bool {
		final speed = 300.0;
		r_small = Math.max(0, r_small - dt * speed);
		r_big = Math.max(0, r_big - dt * speed * 1.3);
		g.clear();
		drawCircles();
		if (r_small > 0 || r_big > 0) {
			return true;
		}
		spotlight.removeChild(g);
		Main.game.layers.removeChild(rulerAnim);
		g = null;
		return false;
	}

	private function removeRulerFromRoom(dt:Float):Bool {
		room.removeItem(Data.ItemKind.bedroom_ruler);
		return false;
	}

	private var playAnim:PlayAnim;
	private var talk:Talk;
	private final ruler:Bitmap;
	private var rulerAnim:Anim;
	private final helen:Anim;
	private final raymond:Anim;
	private final spotlight:Bitmap;
	private final room:BedroomDay;
	private final actor:Actor;
	private final background:Bitmap = new Bitmap(hxd.Res.pirate_bedroom.toTile());
	private var g:Graphics;
	private var helen_width = 130.0;
	private var ramon_x = 100.0;
	private var ramon_y = 320.0;
	private final sleep_x = 675;
	private final sleep_y = 280;
	private var r_big = 340.0 / 2;
	private var r_small = 260.0 / 2;
}
