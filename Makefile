# SPDF-FileCopyrightText: 2022 jordi fita mas <jfita@peritasoft.com>
# 
# SPDX-License-Identifier: GPL-3.0-only

NULL :=
SPACE := $(NULL) #

SOURCES := $(wildcard src/*.hx) $(wildcard src/**/*.hx)

BEDROOM_IDS := $(shell xml select --template --value-of '/svg:svg/svg:g/svg:*/@id' src/items/bedroom.svg)
SPEECH_IDS := $(shell xml select --template --value-of '/svg:svg/svg:g/svg:*/@id' src/items/speech.svg)
ITEMS_IDS:= $(BEDROOM_IDS) $(SPEECH_IDS)
ITEMS_PNG := \
	$(patsubst %,src/items/bedroom_%.png,$(BEDROOM_IDS)) \
	$(patsubst %,src/items/speech_%.png,$(SPEECH_IDS)) \
	$(NULL)

HELEN_IDS := $(shell xml select --template --value-of '/svg:svg/svg:g/svg:*/@id' src/actors/helen.svg)
RAMON_IDS := $(shell xml select --template --value-of '/svg:svg/svg:g/svg:*/@id' src/actors/ramon.svg)
RAYMOND_IDS := $(shell xml select --template --value-of '/svg:svg/svg:g/svg:*/@id' src/actors/raymond.svg)
ACTORS_IDS := $(HELEN_IDS) $(RAMON_IDS) $(RAYMOND_IDS)
ACTORS_PNG := \
	$(patsubst %,src/actors/helen_%.png,$(HELEN_IDS)) \
	$(patsubst %,src/actors/ramon_%.png,$(RAMON_IDS)) \
	$(patsubst %,src/actors/raymond_%.png,$(RAYMOND_IDS)) \
	$(NULL)

THIEF_IDS := $(shell xml select --template --value-of '/svg:svg/svg:g/svg:*/@id' src/thief/scene.svg)
THIEF_PNG := $(patsubst %,src/thief/scene_%.png,$(THIEF_IDS))
PIRATE_IDS := $(shell xml select --template --value-of '/svg:svg/svg:g/svg:*/@id' src/pirate/scene.svg)
PIRATE_PNG := $(patsubst %,src/pirate/scene_%.png,$(PIRATE_IDS))
SCENES_IDS := $(THIEF_IDS) $(PIRATE_IDS)

THIEF_BEDROOM = res/thief_bedroom.png
PIRATE_BEDROOM = res/pirate_bedroom.png
HOSPITAL = res/hospital.png

FONTS := \
	res/AmaticSC.fnt \
	res/AmaticSC.png \
	$(NULL)

ITEMS_ATLAS := \
	res/items.atlas \
	res/items.png \
	$(NULL)

ACTORS_ATLAS := \
	res/actors.atlas \
	res/actors.png \
	$(NULL)

THIEF_ATLAS := \
	res/thief.atlas \
	res/thief.png \
	$(NULL)

PIRATE_ATLAS := \
	res/pirate.atlas \
	res/pirate.png \
	$(NULL)

RESOURCES := \
	$(FONTS) \
	$(ITEMS_ATLAS) \
	$(ACTORS_ATLAS) \
	$(THIEF_ATLAS) \
	$(PIRATE_ATLAS) \
	$(THIEF_BEDROOM) \
	$(PIRATE_BEDROOM) \
	$(HOSPITAL) \
	$(NULL)

BIN_HL := bin/ndjam9.hl

.PHONY: all
all:  $(RESOURCES) $(BIN_HL)

bin/ndjam9.%: ndjam9.%.hxml ndjam9.hxml $(SOURCES) | $(RESOURCES)
	haxe --connect 6000 $< || haxe $<

FONTS_TIMESTAMP := res/fonts.timestamp
.INTERMEDIATE: $(FONTS_TIMESTAMP)
$(FONTS): $(FONTS_TIMESTAMP)
$(FONTS_TIMESTAMP): fonts.json src/Amatic-Bold.ttf
	fontgen $<
	touch $@


ITEMS_ATLAS_TIMESTAMP := res/items_atlas.timestamp
.INTERMEDIATE: $(ITEMS_ATLAS_TIMESTAMP)
$(ITEMS_ATLAS): $(ITEMS_ATLAS_TIMESTAMP)
$(ITEMS_ATLAS_TIMESTAMP): $(ITEMS_PNG)

ACTORS_ATLAS_TIMESTAMP := res/actors_atlas.timestamp
.INTERMEDIATE: $(ACTORS_ATLAS_TIMESTAMP)
$(ACTORS_ATLAS): $(ACTORS_ATLAS_TIMESTAMP)
$(ACTORS_ATLAS_TIMESTAMP): $(ACTORS_PNG)

res/%_atlas.timestamp: src/%/pack.json
	texturepacker src/$* res $*
	touch $@

BEDROOM_ITEMS_TIMESTAMP := res/bedroom_items.timestamp 
SPEECH_ITEMS_TIMESTAMP := res/speech_items.timestamp
ITEMS_TIMESTAMPS := $(BEDROOM_ITEMS_TIMESTAMP) $(SPEECH_ITEMS_TIMESTAMP)
.INTERMEDIATE: $(ITEMS_TIMESTAMPS) $(ITEMS_PNG)
$(ITEMS_PNG): $(ITEMS_TIMESTAMPS)

res/%_items.timestamp: src/items/%.svg
	inkscape --export-id="$(subst $(SPACE),;,$(strip $(ITEMS_IDS)))" --export-id-only --export-type="png" $<
	touch $@

HELEN_ACTOR_TIMESTAMP := res/helen_actors.timestamp
RAMON_ACTOR_TIMESTAMP := res/ramon_actors.timestamp
RAYMOND_ACTOR_TIMESTAMP := res/raymond_actors.timestamp
ACTOR_TIMESTAMPS := $(HELEN_ACTOR_TIMESTAMP) $(RAMON_ACTOR_TIMESTAMP) $(RAYMOND_ACTOR_TIMESTAMP)
.INTERMEDIATE: $(ACTOR_TIMESTAMPS) $(ACTORS_PNG)
$(ACTORS_PNG): $(ACTOR_TIMESTAMPS)

res/%_actors.timestamp: src/actors/%.svg
	inkscape --export-id="$(subst $(SPACE),;,$(strip $(ACTORS_IDS)))" --export-id-only --export-type="png" $<
	touch $@

THIEF_ATLAS_TIMESTAMP := res/thief_atlas.timestamp
.INTERMEDIATE: $(THIEF_ATLAS_TIMESTAMP)
$(THIEF_ATLAS): $(THIEF_ATLAS_TIMESTAMP)
$(THIEF_ATLAS_TIMESTAMP): $(THIEF_PNG)

PIRATE_ATLAS_TIMESTAMP := res/pirate_atlas.timestamp
.INTERMEDIATE: $(PIRATE_ATLAS_TIMESTAMP)
$(PIRATE_ATLAS): $(PIRATE_ATLAS_TIMESTAMP)
$(PIRATE_ATLAS_TIMESTAMP): $(PIRATE_PNG)

res/%_atlas.timestamp: src/%/pack.json
	texturepacker src/$* res $*
	touch $@

THIEF_TIMESTAMP := res/thief.timestamp
$(THIEF_ATLAS): $(THIEF_TIMESTAMP)
.INTERMEDIATE: $(THIEF_TIMESTAMP) $(THIEF_PNG)
$(THIEF_PNG): $(THIEF_TIMESTAMP)

PIRATE_TIMESTAMP := res/pirate.timestamp
$(PIRATE_ATLAS): $(PIRATE_TIMESTAMP)
.INTERMEDIATE: $(PIRATE_TIMESTAMP) $(PIRATE_PNG)
$(PIRATE_PNG): $(PIRATE_TIMESTAMP)

res/%.timestamp: src/%/scene.svg
	inkscape --export-id="$(subst $(SPACE),;,$(strip $(SCENES_IDS)))" --export-id-only --export-type="png" $<
	touch $@

res/%_bedroom.png: src/%/bedroom.svg
	inkscape --export-id="thief" --export-id-only --export-type="png" --export-filename="$@" $<

$(HOSPITAL): src/end/hospital.svg
	inkscape --export-id="hospital" --export-id-only --export-type="png" --export-filename="$@" $<

.PHONY: watch
watch:
	while true; do inotifywait --recursive --event modify src; $(MAKE); done

.PHONY: clean
clean:
	$(RM) $(BIN_HL) $(BIN_JS)
	$(RM) $(RESOURCES)
	$(RM) -r res/.tmp
